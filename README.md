# MentionArea

This is a Vue-based rich text editor that allows for the mentioning of users using first or full names. It keeps a list of every mention in the editor and synchronizes them by listening to `input` events described in [the InputEvent interface document](https://rawgit.com/w3c/input-events/v1/index.html#interface-InputEvent-Attributes).

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
